lof=logfile.txt
dir=/Users/admin/logs/

mkdir $dir
cd $dir

printf "uxer: %s \n" $USER > $lof
date "+date: %D"  >> $lof
date "+time: %T" >> $lof

printf "enable guest user...  " >> $lof
sysadminctl -guestAccount on
printf "finished \n" >> $lof

printf "install homebrew packages... " >> $lof
brew install git wget mas
printf "finished \n" >> $lof

printf "install homebrew cask packages... " >> $lof
brew install --cask firefox eloston-chromium microsoft-teams bluej libreoffice microsoft-remote-desktop rectangle 
printf "finished \n" >> $lof
